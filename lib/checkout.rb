# frozen_string_literal: true

require_relative 'shelf'

Item = Struct.new(:code, :name, :price)
# Thes checkout implement a checkout system that scan products and send back the total price with reduction
class Checkout
  def initialize(*promotional_rules)
    @rules  = promotional_rules
    @basket = {}
    @shelf  = Shelf.new
    @shelf.add(Item.new('001', 'Lavender heart', 9.25))
    @shelf.add(Item.new('002', 'Personalised cufflinks', 45.00))
    @shelf.add(Item.new('003', 'Kids T-shirt', 19.95))
  end

  def scan(item_name)
    code = @shelf.dictionary[item_name]
    @basket[code] ||= 0
    @basket[code] += 1
  end

  def total
    result = price
    reduction = 0

    @rules.each do |rule|
      reduction = 0.10 if rule_one_respected(rule, result)
      @shelf.products['001'].price = 8.5 if rule_two_respected(rule)
    end

    ((price * (1 - reduction)) * 100).round.to_f / 100
  end

  def price
    @basket.map do |list|
      @shelf.products[list[0].to_s].price * list[1]
    end.sum
  end

  private

  def rule_one_respected(rule, result)
    rule == 1 && result > 60
  end

  def rule_two_respected(rule)
    @shelf.products['001'].price = 8.5 if rule == 2 && @basket['001'] && @basket['001'] >= 2
  end
end
