# frozen_string_literal: true

# This represent the list of products currently available on the platform
class Shelf
  attr_reader :dictionary

  def initialize
    @shelf = {}
    @dictionary = {}
  end

  def add(item)
    @shelf[item.code] = item
    @dictionary[item.code] = item.name
    @dictionary[item.name] = item.code
  end

  def remove(code)
    @shelf.delete(code)
    name = dictionary[code]
    @dictionary.delete(code)
    @dictionary.delete(name)
  end

  def products
    @shelf
  end
end
