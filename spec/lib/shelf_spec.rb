# frozen_string_literal: true

require('shelf')

RSpec.describe(Shelf) do
  describe '#add' do
    it 'adds product to the shelf' do
      shelf = Shelf.new
      shelf.add(Item.new('fake_code', 'Fake name', 111))

      expect(shelf.instance_variable_get(:@shelf)['fake_code'].nil?).to eq(false)
    end
  end

  describe '#remove' do
    it 'removes product from the shelf' do
      shelf = Shelf.new
      shelf.add(Item.new('fake_code', 'Fake name', 111))
      shelf.remove('fake_code')

      expect(shelf.products['fake_code'].nil?).to eq(true)
    end
  end

  describe '#products' do
    it 'give the products on the shelf' do
      shelf = Shelf.new
      shelf.add(Item.new('fake_code', 'Fake name', 111))

      expect(shelf.products['fake_code'].nil?).to eq(false)
    end
  end

  describe '#dictionary' do
    it 'translate code to name and name to codea' do
      shelf = Shelf.new
      shelf.add(Item.new('fake_code', 'Fake name', 111))

      expect(shelf.dictionary['fake_code']).to eq('Fake name')
      expect(shelf.dictionary['Fake name']).to eq('fake_code')
    end
  end
end
