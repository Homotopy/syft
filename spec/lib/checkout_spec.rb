# frozen_string_literal: true

require('checkout')

RSpec.describe(Checkout) do
  # This tests are executed multiple times to garanty that the computation is valid in differents configurations
  #
  describe '#price' do
    let(:quantity1) { rand(0..9) }
    let(:quantity2) { rand(0..9) }
    let(:quantity3) { rand(0..9) }

    it 'compute the accurate price' do
      co = Checkout.new(1)

      quantity1.times do
        co.scan('Lavender heart')
      end

      quantity2.times do
        co.scan('Personalised cufflinks')
      end

      quantity3.times do
        co.scan('Kids T-shirt')
      end

      result = quantity1 * 9.25 + quantity2 * 45 + quantity3 * 19.95

      expect(co.price).to eq(result)
    end
  end

  # This tests are executed multiple times to garanty that the computation is valid in differents configurations
  #
  describe '#total' do
    1.upto(50) do
      let(:quantity1) { rand(2..9) }
      let(:quantity2) { rand(2..9) }
      let(:quantity3) { rand(2..9) }

      it 'compute the accurate total with no reduction' do
        co = Checkout.new

        quantity1.times do
          co.scan('Lavender heart')
        end
        quantity2.times do
          co.scan('Personalised cufflinks')
        end
        quantity3.times do
          co.scan('Kids T-shirt')
        end

        result = ((quantity1 * 9.25 + quantity2 * 45 + quantity3 * 19.95) * 100).round.to_f / 100

        expect(co.total).to eq(result)
      end

      it 'compute the accurate total with condition for reduction 1 valid' do
        co = Checkout.new(1)

        quantity1.times do
          co.scan('Lavender heart')
        end

        quantity2.times do
          co.scan('Personalised cufflinks')
        end

        quantity3.times do
          co.scan('Kids T-shirt')
        end

        result = ((quantity1 * 9.25 + quantity2 * 45 + quantity3 * 19.95) * 0.9 * 100).round.to_f / 100

        expect(co.total).to eq(result)
      end

      it 'compute the accurate total with condition for reduction 1 not valid' do
        co = Checkout.new(1)

        co.scan('Lavender heart')
        co.scan('Personalised cufflinks')

        expect(co.total).to eq(54.25)
      end

      it 'compute the accurate total with condition for reduction 2 valid' do
        co = Checkout.new(2)

        quantity1.times do
          co.scan('Lavender heart')
        end
        quantity2.times do
          co.scan('Personalised cufflinks')
        end
        quantity3.times do
          co.scan('Kids T-shirt')
        end

        result = ((quantity1 * 8.5 + quantity2 * 45 + quantity3 * 19.95) * 100).round.to_f / 100

        expect(co.total).to eq(result)
      end

      it 'compute the accurate total with condition for reduction 2 not valid' do
        co = Checkout.new(2)

        co.scan('Lavender heart')
        co.scan('Personalised cufflinks')
        co.scan('Personalised cufflinks')
        co.scan('Kids T-shirt')

        expect(co.total).to eq(119.2)
      end

      it 'compute the accurate total with conditions for reduction 1 2 valid' do
        co = Checkout.new(1, 2)

        quantity1.times do
          co.scan('Lavender heart')
        end
        quantity2.times do
          co.scan('Personalised cufflinks')
        end
        quantity3.times do
          co.scan('Kids T-shirt')
        end

        result = ((quantity1 * 8.5 + quantity2 * 45 + quantity3 * 19.95) * 0.9 * 100).round.to_f / 100

        expect(co.total).to eq(result)
      end

      it 'compute the accurate total with conditions for reduction 1 and 2 not valid' do
        co = Checkout.new(1, 2)

        co.scan('Lavender heart')
        co.scan('Kids T-shirt')
        co.scan('Kids T-shirt')

        expect(co.total).to eq(49.15)
      end
    end
  end

  describe '#scan' do
    let(:quantity1) { rand(1..9) }
    let(:quantity2) { rand(1..9) }
    let(:quantity3) { rand(1..9) }

    it 'add items to basket' do
      co = Checkout.new(1)
      quantity1.times do
        co.scan('Lavender heart')
      end

      quantity2.times do
        co.scan('Personalised cufflinks')
      end

      quantity3.times do
        co.scan('Kids T-shirt')
      end

      expect(co.instance_variable_get(:@basket)).to eq({ '001' => quantity1, '002' => quantity2, '003' => quantity3 })
    end
  end

  describe 'original test' do
    it 'add items Lavender heart,Personalised cufflinks,Kids T-shirt' do
      co = Checkout.new(1)

      co.scan('Lavender heart')
      co.scan('Personalised cufflinks')
      co.scan('Kids T-shirt')

      expect(co.total).to eq(66.78)
    end

    it 'add items Lavender heart,Kids T-shirt,Lavender heart' do
      co = Checkout.new(1, 2)

      co.scan('Lavender heart')
      co.scan('Kids T-shirt')
      co.scan('Lavender heart')

      expect(co.total).to eq(36.95)
    end

    it 'add items Lavender heart,Personalised cufflinks,Lavender heart,Kids T-shirt' do
      co = Checkout.new(1, 2)

      co.scan('Lavender heart')
      co.scan('Personalised cufflinks')
      co.scan('Lavender heart')
      co.scan('Kids T-shirt')

      expect(co.total).to eq(73.76)
    end
  end
end
