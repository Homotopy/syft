# syft

This is a test for Syft

It uses ruby `2.5.7`, but it should work with any version ruby after `2.0.0`.


```ruby
bundle console

require "./lib/checkout"

co = Checkout.new(..., rule_code)

co.scan(item_name)
co.scan(item_name)

price = co.total
```

We use this informations as defintions

```ruby

Product code  | Name                   | Price
----------------------------------------------------------
001           | Lavender heart         | £9.25
002           | Personalised cufflinks | £45.00
003           | Kids T-shirt           | £19.95


rules code  | rules goals                   
--------------------------------------------------------------------------------
1           | If you spend over £60, then you get 10% of your purchase        
2           | If you buy 2 or more lavender hearts then the price drops to £8.50
```
